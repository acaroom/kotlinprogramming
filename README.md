# Do it! Kotlin Programming
Examples for 'Do it! Kotlin Programming'

## Introduction

Kotlin has been around for a few years and running on lots of products. Kotlin is became a very useful and powerful language in modern programming world. There is no doubt that Kotlin is better than Java. It is much safer and more concise. It provides a programmer with a bunch of modern skills and mothods. Those include things like null safety, extension functions, data classes, objects, first class function as well as extensive lambdas. 

I wrote the 'Do it! Kotlin Programming'. In this textbook, I will cover all of new features for Kotlin like syntax, functions, classes and so on. Also, I provide lots of basic examples for you. 

## 소개

코틀린은 몇년동안 많은 제품에 적용되어져 오면서 현대 프로그래밍 세계에서 빼놓을 수 없는 유용하고 강력한 언어가 되었습니다. 코틀린이 자바보다 더 유용하다는데 의심의 여지가 없을 것입니다. 코틀린은 더 안전하고 더 간략화된 특징을 가지고 있죠. 프로그래머가 최근의 프로그래밍 스킬과 방법을 구사할 수 있도록 널 안전성이나 확장함수, 데이터 클래스, 객체 표현이나 일급 클래스 함수, 람다식등을 제공하고 있습니다. 

두잇! 코틀린 프로그래밍을 쓰면서 많은 예제를 통해 문법, 함수, 클래스등의 모든 기능을 다룰것입니다. 예제는 언제든 다운로드 받을 수 있도록 여러분들에게 제공합니다. 


